// Copyright 2019 Signal Digital, Inc. All rights reserved.
package SeleniumTests;

import java.lang.reflect.Method;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.selenium.Eyes;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.LoginFormPage;

/**
 * @author icimpean
 * @since 11/13/2019
 */
public class VisualAITests extends BaseTest {

  private Eyes eyes = new Eyes();

  @BeforeClass
  public void beforeClass() {
    eyes.setApiKey("qmXZNPwU7KUe1PlYvjVrGAWTHPOiHxGYyq5F1hIk1D4110");
    eyes.setBatch(new BatchInfo("Hackathon"));
  }

  @Parameters( {"url"})
  @BeforeMethod
  public void beforeSetup(Method method, @Optional("https://demo.applitools.com/hackathon.html") String url) {
    driver.get(url);
    loginFormPage = new LoginFormPage(driver);
    eyes.open(driver, "Applitool-app", method.getName());
  }

  @AfterMethod
  public void tearDown() {
    eyes.closeAsync();
    if (eyes != null)
      eyes.abortIfNotClosed();
  }

  @Test
  public void checkLoginPage() {
    eyes.checkWindow("Login Form");
  }

  @Test(priority = 1)
  public void loginWithInvalidCredentials() {
    loginFormPage.clickLoginButton();
    eyes.checkWindow(" Invalid Login Form");
    loginFormPage.enterUsername("random");
    loginFormPage.clickLoginButton();
    eyes.checkWindow("Invalid Login Form");
    loginFormPage.enterUsername("");
    loginFormPage.enterPassword("random");
    loginFormPage.clickLoginButton();
    eyes.checkWindow("Invalid Login Form");
  }

  @Test(priority = 2)
  public void loginWithValidCredentials() {
    indexPage = login();
    eyes.checkWindow("Success login");
  }

  @Test(priority = 3)
  public void checkIfTableIsSored() {
    indexPage = login();
    eyes.checkWindow("Table - initial state");
    indexPage.clickAmountHeader();
    eyes.checkWindow("Table sorted");
  }

  @Test(priority = 4)
  public void checkIfAdsAreDisplayed() {
    driver.get(driver.getCurrentUrl() + "?showAd=true");
    indexPage = login();
    eyes.checkWindow("Displayed Ads");
  }

  @Test(priority = 5)
  public void checkBarGraphicsPage() {
    indexPage = login();
    compareExpressPage = indexPage.clickCompareExpensesLink();
    eyes.checkWindow("Bar Graph - initial state");
    compareExpressPage.clickAddDataForNextYear();
    eyes.checkWindow("Bar Graph - with next year data");
  }

}