package SeleniumTests;

import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pages.LoginFormPage;

public class TraditionalTests extends BaseTest {

  private SoftAssert softAssertion= new SoftAssert();

  @Parameters( {"url"})
  @BeforeMethod
  public void beforeSetup(@Optional("https://demo.applitools.com/hackathon.html") String url) {
    driver.get(url);
    loginFormPage = new LoginFormPage(driver);
  }

  @Test()
  public void checkLoginPage(){
    softAssertion.assertTrue(loginFormPage.getFormName().equals("Login Form"),"Wrong form name");
    softAssertion.assertTrue(loginFormPage.isUsernamePlaceholderCorrect(),"Wrong placeholder for username");
    softAssertion.assertTrue(loginFormPage.isPasswordPlaceholderCorrect(),"Wrong placeholder for password");
    softAssertion.assertTrue(loginFormPage.isUsernameLabelCorrect(),"Wrong label for username");
    softAssertion.assertTrue(loginFormPage.isPasswordLabelCorrect(),"Wrong label for password");
    softAssertion.assertTrue(loginFormPage.isUsernameIconDisplayed(),"Username icon is not displayed");
    softAssertion.assertTrue(loginFormPage.isPasswordIconDisplayed(),"Password icon is not displayed");
    softAssertion.assertTrue(loginFormPage.isFacebookrIconDisplayed(),"Facebook icon is not displayed");
    softAssertion.assertTrue(loginFormPage.isLinkedinIconDisplayed(),"Linkedin icon is not displayed");
    softAssertion.assertTrue(loginFormPage.isTwitterIconDisplayed(),"Twitter icon is not displayed");
    softAssertion.assertAll();
  }

  @Test(priority = 1)
  public void loginWithInvalidCredentials(){
    loginFormPage.clickLoginButton();
    softAssertion.assertTrue(loginFormPage.getWarningText().equals("Both Username and Password must be present"),"Warning message is not the expected one");
    loginFormPage.enterUsername("random");
    loginFormPage.clickLoginButton();
    softAssertion.assertTrue(loginFormPage.getWarningText().equals("Password must be present"),"Warning message is not the expected one");
    loginFormPage.enterUsername("");
    loginFormPage.enterPassword("random");
    loginFormPage.clickLoginButton();
    softAssertion.assertTrue(loginFormPage.getWarningText().equals("Username must be present"),"Warning message is not the expected one");
    softAssertion.assertAll();
  }

  @Test(priority = 2)
  public void loginWithValidCredentials() {
    indexPage = login();
    softAssertion.assertTrue(indexPage.isIndexPageDisplayed(),"Could not log in");
    softAssertion.assertAll();
  }

  @Test(priority = 3)
  public void checkIfTableIsSorted(){
    indexPage = login();
    List<String> beforeSortList = indexPage.getTableContents();
    indexPage.clickAmountHeader();
    List<String> afterSortList = indexPage.getTableContents();
    softAssertion.assertTrue(beforeSortList.containsAll(afterSortList),"Table content is different");
    softAssertion.assertTrue(indexPage.checkIfListIsSortedAscending(),"the list isn't sorted");
    softAssertion.assertAll();
  }

  @Test(priority = 4)
  public void checkIfAdsAreDisplayed(){
    driver.get(driver.getCurrentUrl() + "?showAd=true");
    indexPage = login();
    softAssertion.assertTrue(indexPage.isFirstAdDisplayed(),"First ad is not displayed");
    softAssertion.assertTrue(indexPage.isSecondAdDisplayed(),"Second ad is not displayed");
    softAssertion.assertAll();
  }

  @Test(priority = 5)
  public void checkBarGraphicsPage() {
    indexPage = login();
    compareExpressPage = indexPage.clickCompareExpensesLink();
    // did not implement because there is no straight-forward way to interact with the Graph Element
    compareExpressPage.clickAddDataForNextYear();
    // did not implement because there is no DOM change when the new data appears
  }



}
