package SeleniumTests;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import pages.CompareExpressPage;
import pages.IndexPage;
import pages.LoginFormPage;

@Test
public class BaseTest {
    protected RemoteWebDriver driver;
    public LoginFormPage loginFormPage;
    public IndexPage indexPage;
    public CompareExpressPage compareExpressPage;

    @BeforeTest
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        System.out.println(" Running on " + driver.getCapabilities().getBrowserName() + " " + driver.getCapabilities().getVersion());
    }

    @AfterTest
    public void tearDown() {
        if (driver != null)
            driver.quit();
    }

    public IndexPage login(){
        loginFormPage.enterUsername("random");
        loginFormPage.enterPassword("random");
        loginFormPage.clickLoginButton();
        return new IndexPage(driver);
    }
}
