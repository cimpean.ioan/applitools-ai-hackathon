package resources;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utility {

  public WebDriver driver;
  public WebDriverWait wait;

  public Utility(WebDriver driver) {
    this.driver = driver;
    wait = new WebDriverWait(driver, 25);
  }

  public <V> V optionalWaitUntil(int seconds, com.google.common.base.Function<? super WebDriver, V> condition) {
    wait = new WebDriverWait(driver, seconds);
    try {
      WebDriverWait optionalWait = new WebDriverWait(driver, seconds);
      return optionalWait.until(condition);
    } catch (Exception e) {
      return null;
    }
  }

  public List<WebElement> optionalWaitForList(int seconds, com.google.common.base.Function<? super WebDriver, List<WebElement>> condition) {
    wait = new WebDriverWait(driver, seconds);
    try {
      WebDriverWait optionalWait = new WebDriverWait(driver, seconds);
      List<WebElement> listElement = optionalWait.until(condition);
      return listElement;
    } catch (Exception e) {
      return Collections.emptyList();
    }
  }

  public void navigateTo(String URL) {
    driver.navigate().to(URL);
  }

  public void sendKeys(WebElement element, String text) {
    element.click();
    element.clear();
    element.sendKeys(text);
  }

  public static Comparator<String> createNaturalOrderRegexComparator() {
    return Comparator.comparingDouble(NaturalOrderComparators::parseStringToNumber);
  }

}