package pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import resources.NaturalOrderComparators;

public class IndexPage extends AbstractPageObject {

  public static final String COMPARE_EXPENSES_LINK_ID = "showExpensesChart";
  public static final String AMOUNT_HEADER_ID = "amount";
  public static final String AMOUNT_TABLE_CELLS_XPATH = "//td[contains(@class,'text-right')]";
  public static final String FIRST_AD_XPATH = "//div[@id='flashSale']/img";
  public static final String SECOND_AD_XPATH = "//div[@id='flashSale2']/img";
  public static final String TABLE_ROWS_CSS = "#transactionsTable tbody tr";

  @FindBy(id = COMPARE_EXPENSES_LINK_ID)
  public WebElement compareExpensesLink;

  @FindBy(id = AMOUNT_HEADER_ID)
  public WebElement amountHeader;

  @FindBy(xpath = AMOUNT_TABLE_CELLS_XPATH)
  public List<WebElement> amountTableCells;

  @FindBy(css = TABLE_ROWS_CSS)
  public List<WebElement> tableRows;

  public IndexPage(WebDriver driver) {
    super(driver);
  }

  public CompareExpressPage clickCompareExpensesLink() {
    compareExpensesLink.click();
    return new CompareExpressPage(driver);
  }

  public void clickAmountHeader() {
    amountHeader.click();
  }

  public boolean isFirstAdDisplayed() {
    return utils.optionalWaitUntil(3,
        ExpectedConditions.visibilityOfElementLocated(By.xpath(FIRST_AD_XPATH)))!=null;
  }

  public boolean isSecondAdDisplayed() {
    return utils.optionalWaitUntil(3,
        ExpectedConditions.visibilityOfElementLocated(By.xpath(SECOND_AD_XPATH)))!=null;
  }

  public boolean checkIfListIsSortedAscending() {
    ArrayList<String> obtainedList = new ArrayList<>();

    for (WebElement we : amountTableCells) {
      obtainedList.add(we.getText());
    }
    ArrayList<String> sortedList = new ArrayList<>();
    for (String s : obtainedList) {
      sortedList.add(s);
    }
    sortedList = sortBySignAndValue(obtainedList);
    return sortedList.equals(obtainedList);
  }

  public boolean isIndexPageDisplayed(){
    return compareExpensesLink.isDisplayed();
  }

  public ArrayList<String> sortBySignAndValue(ArrayList<String> initialList) {
    ArrayList<String> negativeNumbers = new ArrayList<>();
    ArrayList<String> positiveNumbers = new ArrayList<>();
    for (String currentNumber : initialList) {
      if (currentNumber.substring(0,1).equalsIgnoreCase("-"))
        negativeNumbers.add(currentNumber);
      else
        positiveNumbers.add(currentNumber);
    }
    /* "magic sort" = sort only double numbers (strip off extra chars) */
    negativeNumbers.sort(NaturalOrderComparators.createNaturalOrderRegexComparator());
    positiveNumbers.sort(NaturalOrderComparators.createNaturalOrderRegexComparator());
    /* end of "magic sort" */
    // we need to reverse this since biggest number with negative sign will actually be the smallest
    Collections.reverse(negativeNumbers);

    ArrayList<String> sortedBySignList = new ArrayList<>();
    sortedBySignList.addAll(negativeNumbers);
    sortedBySignList.addAll(positiveNumbers);
    return sortedBySignList;
  }

  public List<String> getTableContents(){
    List<String> tableRow = new LinkedList<>();
    for (int i =0;i<tableRows.size();i++)
      tableRow.add(tableRows.get(i).getText());
    return tableRow;
  }

  @Override
  public void waitForPageToLoad() {
    utils.wait.until(ExpectedConditions.visibilityOf(compareExpensesLink));
  }

}
