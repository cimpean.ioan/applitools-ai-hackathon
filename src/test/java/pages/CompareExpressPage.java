package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CompareExpressPage extends AbstractPageObject{

  public static final String DATA_CHART_ID = "canvas";
  public static final String ADD_DATA_FOR_NEXT_YEAR_ID = "addDataset";

  @FindBy(id = ADD_DATA_FOR_NEXT_YEAR_ID)
  public WebElement addDataForNextYearButton;

  public CompareExpressPage(WebDriver driver) {
    super(driver);
  }

  public void clickAddDataForNextYear(){
    addDataForNextYearButton.click();
  }

  @Override
  public void waitForPageToLoad() {
    utils.wait.until(ExpectedConditions.visibilityOf(addDataForNextYearButton));
  }

}
