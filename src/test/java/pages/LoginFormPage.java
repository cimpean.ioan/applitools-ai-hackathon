package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginFormPage extends AbstractPageObject {

  public static final String LOGIN_FORM_CLASS = "auth-header";
  public static final String USERNAME_FIELD_ID = "username";
  public static final String PASSWORD_FIELD_ID = "password";
  public static final String LOG_IN_BUTTON_ID = "log-in";
  public static final String USERNAME_ICON_CLASS = "os-icon-user-male-circle";
  public static final String PASSWORD_ICON_CLASS = "os-icon-fingerprint";
  public static final String LINKEDIN_ICON_XPATH = "//img[@src='img/social-icons/linkedin.png']";
  public static final String FACEBOOK_ICON_XPATH = "//img[@src='img/social-icons/facebook.png']";
  public static final String TWITTER_ICON_XPATH = "//img[@src='img/social-icons/twitter.png']";
  public static final String USERNAME_LABEL_XPATH = "//input[@id='username']/preceding-sibling::label";
  public static final String PASSWORD_LABEL_XPATH = "//input[@id='password']/preceding-sibling::label";
  public static final String LOGIN_ERROR_WARNING_CLASS = "alert-warning";

  @FindBy(className = LOGIN_FORM_CLASS)
  public WebElement loginForm;

  @FindBy(id = USERNAME_FIELD_ID)
  public WebElement usernameField;

  @FindBy(id = PASSWORD_FIELD_ID)
  public WebElement passwordField;

  @FindBy(id = LOG_IN_BUTTON_ID)
  public WebElement logInButton;

  @FindBy(xpath = TWITTER_ICON_XPATH)
  public WebElement twitterIcon;

  @FindBy(xpath = FACEBOOK_ICON_XPATH)
  public WebElement facebookIcon;

  @FindBy(xpath = USERNAME_LABEL_XPATH)
  public WebElement usernameLabel;

  @FindBy(xpath = PASSWORD_LABEL_XPATH)
  public WebElement passwordLabel;

  @FindBy(className = LOGIN_ERROR_WARNING_CLASS)
  public WebElement loginWarning;

  public LoginFormPage(WebDriver driver) {
    super(driver);
  }

  public void enterUsername(String username) {
    usernameField.clear();
    usernameField.sendKeys(username);
  }

  public void enterPassword(String password) {
    passwordField.clear();
    passwordField.sendKeys(password);
  }

  public String getFormName() {
    return loginForm.getText();
  }

  public void clickLoginButton() {
    logInButton.click();
  }

  public boolean isUsernameIconDisplayed() {
    return utils.optionalWaitUntil(3,
        ExpectedConditions.visibilityOfElementLocated(By.className(USERNAME_ICON_CLASS)))!=null;
  }

  public boolean isPasswordIconDisplayed() {
    return utils.optionalWaitUntil(3,
        ExpectedConditions.visibilityOfElementLocated(By.className(PASSWORD_ICON_CLASS)))!=null;
  }

  public boolean isUsernamePlaceholderCorrect() {
    return usernameField.getAttribute("placeholder").equals("Enter your username");
  }

  public boolean isPasswordPlaceholderCorrect() {
    return passwordField.getAttribute("placeholder").equals("Enter your password");
  }

  public boolean isTwitterIconDisplayed() {
    return twitterIcon.isDisplayed();
  }

  public boolean isFacebookrIconDisplayed() {
    return facebookIcon.isDisplayed();
  }

  public boolean isLinkedinIconDisplayed() {
    return utils.optionalWaitUntil(3,
        ExpectedConditions.visibilityOfElementLocated(By.xpath(LINKEDIN_ICON_XPATH)))!=null;
  }

  public boolean isUsernameLabelCorrect() {
    return usernameLabel.getText().equals("Username");
  }

  public boolean isPasswordLabelCorrect() {
    return passwordLabel.getText().equals("Password");
  }

  public String getWarningText() {
    return loginWarning.getText();
  }

  @Override
  public void waitForPageToLoad() {
    utils.wait.until(ExpectedConditions.visibilityOfElementLocated(
        By.id(USERNAME_FIELD_ID)));
  }
}